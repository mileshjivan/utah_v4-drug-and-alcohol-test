/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class S5_2_Drug_and_Alcohol_Test_TestSuite {

    static TestMarshall instance;

    public S5_2_Drug_and_Alcohol_Test_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    //S5_2_Drug_and_Alcohol_Test_QA01S5_2
    @Test
    public void S5_2_Drug_and_Alcohol_Test_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_Drug_and_Alcohol_Test_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR1 Capture Drug and Alcohol Test Details - Main Scenario
    @Test
    public void FR1_Capture_Drug_and_Alcohol_Test_Details_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Drug and Alcohol Test Details v5.2\\FR1-Capture Drug and Alcohol Test Details - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Drug_and_Alcohol_Test_Details_Alternate_Scenario_1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Drug and Alcohol Test Details v5.2\\FR1-Capture Drug and Alcohol Test Details - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Drug_and_Alcohol_Test_Details_Alternate_Scenario_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Drug and Alcohol Test Details v5.2\\FR1-Capture Drug and Alcohol Test Details - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
