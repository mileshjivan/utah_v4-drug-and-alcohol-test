/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Drug_and_Alcohol_Test_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Drug_and_Alcohol_Test_V5_2_PageObjects.Drug_and_Alcohol_Test_V5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Drug and Alcohol Test Details v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR1_Capture_Drug_and_Alcohol_Test_Details_MainScenario extends BaseClass {
    String error = "";

    public FR1_Capture_Drug_and_Alcohol_Test_Details_MainScenario() {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Drug_and_Alcohol_Test_Details()) {
            return narrator.testFailed("Capture Resolve Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captureed Drug and Alcohol Test Details");
    }

    public boolean Capture_Drug_and_Alcohol_Test_Details() {
        //Occupational Health
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Occupational_Health_Tab())) {
            error = "Failed to wait for Occupational Health Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Occupational_Health_Tab())) {
            error = "Failed to click on Occupational Health Tab";
            return false;
        }

        //Occupational Health Page
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Drug_Alcohol_Test_Tab())) {
            error = "Failed to wait for Drug & Alcohol Test Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Drug_Alcohol_Test_Tab())) {
            error = "Failed to click on Drug & Alcohol Test Tab";
            return false;
        }

        //Add Button
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Drug_and_Alcohol_Test_add())) {
            error = "Failed to wait for add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Drug_and_Alcohol_Test_add())) {
            error = "Failed to click on add button";
            return false;
        }

        //Process flow
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }

        // Employment Type
        //visible_drop_down_transition
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Employment_Type())) {
            error = "Failed to wait for Employment Type drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Employment_Type())) {
            error = "Failed to click on Employment Type drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Employment Type drop down.");
        

        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition(getData("Employment Type")))) {
            error = "Failed to wait for Employment Type drop down option :" + getData("Employment Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition(getData("Employment Type")))) {
            error = "Failed to click on Employment Type drop down option :" + getData("Employment Type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Employment Type :" + getData("Employment Type"));
        pause(2000);

        //Person being tested  
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Person_being_tested())) {
            error = "Failed to wait for Employment Type drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Person_being_tested())) {
            error = "Failed to click on Person being tested  drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Person being tested  drop down.");
        pause(2000);

        // enter Person being tested
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Person_being_tested_input())) {
            error = "Failed to wait for Person being tested input field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Person_being_tested_input(), getData("Person being tested"))) {
            error = "Failed to enter Person being tested in text field :" + getData("Person being tested");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Person being tested :" + getData("Person being tested"));
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Person being tested")))) {
            error = "Failed to wait for Person being tested drop down option :" + getData("Person being tested");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Person being tested")))) {
            error = "Failed to click on Person being tested drop down option :" + getData("Person being tested");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Person being tested :" + getData("Person being tested"));
        pause(2000);

        //Linked incident
        String Linked_incident = getData("Linked incident");
        if (Linked_incident.equalsIgnoreCase("True")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Linked_incident())) {
                error = "Failed to wait for Linked incident option ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Linked_incident())) {
                error = "Failed to click on Linked incident option ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Linked incident");
            

            //drop down
            if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Linked_incident_drop_down())) {
                error = "Failed to wait for Linked incident drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Linked_incident_drop_down())) {
                error = "Failed to click on Linked incident drop down";
                return false;
            }

            //Linked incident drop down 
            if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Person_being_tested_input())) {
                error = "Failed to wait for Person being tested input field";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Person_being_tested_input(), getData("Linked incident search"))) {
                error = "Failed to enter Linked incident in search field :" + getData("Linked incident search");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully entered Linked incident :" + getData("Linked incident search"));
            

            //search results 
            if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Linked incident search")))) {
                error = "Failed to wait for Linked incident drop down option :" + getData("Linked incident search");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Linked incident search")))) {
                error = "Failed to click on Linked incident drop down option :" + getData("Linked incident search");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Linked incident option :" + getData("Linked incident search"));
            pause(2000);

        }//end of Linked incident if statement

        //Medical test date
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Medical_date())) {
            error = "Failed to wait for Medical test date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Medical_date(), startDate)) {
            error = "Failed to enter Medical test date :" + startDate;
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Medical test date :" + startDate);
        pause(2000);

        //Appointment date  
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Appointment_date())) {
            error = "Failed to wait for Appointment  date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Appointment_date(), endDate)) {
            error = "Failed to enter Appointment date :" + endDate;
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Appointment  date :" + endDate);
        

        //Person submit to a test
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Person_submit())) {
            error = "Failed to wait for Person submit to a test drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Person_submit())) {
            error = "Failed to click for Person submit to a test drop down";
            return false;
        }
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Person submit to a test")))) {
            error = "Failed to wait for Person submit to a test drop down option :" + getData("Person submit to a test");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Person submit to a test")))) {
            error = "Failed to click on Linked incident drop down option :" + getData("Person submit to a test");
            return false;
        }

        //Alcohol test result
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Alcohol_result())) {
            error = "Failed to wait for Alcohol test result drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Alcohol_result())) {
            error = "Failed to click  Alcohol test result drop down";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Alcohol test result")))) {
            error = "Failed to wait for Alcohol test result drop down option :" + getData("Alcohol test result");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Alcohol test result")))) {
            error = "Failed to click on Alcohol test result drop down option :" + getData("Alcohol test result");
            return false;
        }

        //Drug test result
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Drug_result())) {
            error = "Failed to wait for Drug test result drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Drug_result())) {
            error = "Failed to click Drug test result drop down";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Drug test result")))) {
            error = "Failed to wait for Drug test result drop down option :" + getData("Drug test result");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Drug test result")))) {
            error = "Failed to click on Drug test result drop down option :" + getData("Drug test result");
            return false;
        }

        String results = getData("Drug test result");
        if (results.equalsIgnoreCase("Non-Negative")) {
            //Is this measurement due to medication or not prescribed medication?
            if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Prescribed_Medication())) {
                error = "Failed to wait for Is this measurement due to medication or not prescribed medication drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Prescribed_Medication())) {
                error = "Failed to clickIs this measurement due to medication or not prescribed medication drop down";
                return false;
            }
           
            if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Is this measurement due to medication or not prescribed medication")))) {
                error = "Failed to wait for Is this measurement due to medication or not prescribed medication drop down option :" + getData("Is this measurement due to medication or not prescribed medication");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Is this measurement due to medication or not prescribed medication")))) {
                error = "Failed to click on Is this measurement due to medication or not prescribed medication drop down option :" + getData("Is this measurement due to medication or not prescribed medication");
                return false;
            }
        }//end of if

        //Breath
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Breath())) {
            error = "Failed to wait for Breath drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Breath())) {
            error = "Failed to click  Breath drop down";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Breath")))) {
            error = "Failed to wait for Breath drop down option :" + getData("Breath");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Breath")))) {
            error = "Failed to click on  Breath drop down option :" + getData("Breath");
            return false;
        }

        //Speech
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Speech())) {
            error = "Failed to wait for Speech drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Speech())) {
            error = "Failed to click  Speech drop down";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Speech")))) {
            error = "Failed to wait for Speech drop down option :" + getData("Speech");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Speech")))) {
            error = "Failed to click on Speech drop down option :" + getData("Speech");
            return false;
        }

        //Appearance
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Appearance())) {
            error = "Failed to wait for Appearance drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Appearance())) {
            error = "Failed to click Appearance drop down";
            return false;
        }
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Appearance")))) {
            error = "Failed to wait for Appearance drop down option :" + getData("Appearance");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Appearance")))) {
            error = "Failed to click on Appearance drop down option :" + getData("Appearance");
            return false;
        }

        //Could employee stand on one leg?
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.employee_stand())) {
            error = "Failed to wait for Could employee stand on one leg drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.employee_stand())) {
            error = "Failed to click  Could employee stand on one leg drop down";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Could employee stand on one leg")))) {
            error = "Failed to wait for Could employee stand on one leg drop down option :" + getData("Could employee stand on one leg");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Could employee stand on one leg")))) {
            error = "Failed to click on Could employee stand on one leg drop down option :" + getData("Could employee stand on one leg");
            return false;
        }

        //The employee was found FIT/UNFIT for duty
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Duty())) {
            error = "Failed to wait for The employee was found FIT/UNFIT for duty drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Duty())) {
            error = "Failed to click The employee was found FIT/UNFIT for duty drop down";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("The employee was found FIT/UNFIT for duty")))) {
            error = "Failed to wait for The employee was found FIT/UNFIT for duty drop down option :" + getData("The employee was found FIT/UNFIT for duty");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("The employee was found FIT/UNFIT for duty")))) {
            error = "Failed to click on The employee was found FIT/UNFIT for duty drop down option :" + getData("The employee was found FIT/UNFIT for duty");
            return false;
        }

        //Eyes
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Eyes())) {
            error = "Failed to wait for Eyes drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Eyes())) {
            error = "Failed to click Eyes drop down";
            return false;
        }
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Eyes")))) {
            error = "Failed to wait for Eyes drop down option :" + getData("Eyes");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Eyes")))) {
            error = "Failed to click on Eyes drop down option :" + getData("Eyes");
            return false;
        }

        //Behaviour
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Behaviour())) {
            error = "Failed to wait for Behaviour drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Behaviour())) {
            error = "Failed to click Behaviour drop down";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Behaviour")))) {
            error = "Failed to wait for Behaviour drop down option :" + getData("Behaviour");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Behaviour")))) {
            error = "Failed to click on Behaviour drop down option :" + getData("Behaviour");
            return false;
        }

        //Could employee walk in a straight line?
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.employee_walk())) {
            error = "Failed to wait for Could employee walk in a straight line drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.employee_walk())) {
            error = "Failed to click Could employee walk in a straight line drop down";
            return false;
        }
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Could employee walk in a straight line")))) {
            error = "Failed to wait for Could employee walk in a straight line drop down option :" + getData("Could employee walk in a straight line");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Could employee walk in a straight line")))) {
            error = "Failed to click on Could employee walk in a straight line drop down option :" + getData("Could employee walk in a straight line");
            return false;
        }
        //Further action required
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Further_action_required())) {
            error = "Failed to wait for Further action required drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Further_action_required())) {
            error = "Failed to click Further action required drop down";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Further action required")))) {
            error = "Failed to wait for  Further action required drop down option :" + getData("Further action required");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.visible_drop_down_transition_2(getData("Further action required")))) {
            error = "Failed to click on Further action required drop down option :" + getData("Further action required");
            return false;
        }
        //Comments
        if (!SeleniumDriverInstance.waitForElementByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Comments())) {
            error = "Failed to wait for Comments text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.Comments(), getData("Comments"))) {
            error = "Failed to enter Comments in text box :" + getData("Comments");
            return false;
        }

        //Save
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.SaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.SaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Save button");

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Drug_and_Alcohol_Test_V5_2_PageObjects.drugAlcoholTestRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         

        return true;
    }

}
