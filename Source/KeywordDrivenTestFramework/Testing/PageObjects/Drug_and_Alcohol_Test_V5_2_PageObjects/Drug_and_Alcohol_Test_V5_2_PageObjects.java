/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Drug_and_Alcohol_Test_V5_2_PageObjects;

/**
 *
 * @author smabe
 */
public class Drug_and_Alcohol_Test_V5_2_PageObjects {

    public static String Occupational_Health_Tab() {
        return "//div//label[text()='Occupational Health']";
    }

    public static String Drug_Alcohol_Test_Tab() {
        return "//div//label[text()='Drug & Alcohol Test']";
    }

    public static String Drug_and_Alcohol_Test_add() {
        return "//div[text()='Add']";
    }
    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }
    public static String PermitToWork_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_41B8AE76-F4F5-4CA6-97C2-8DCB7A749951']";
    }

    public static String process_flow() {
        return "//div[@id='btnProcessFlow_form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']";
    }

    public static String visible_drop_down_transition(String data) {
        return "//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')]";
    }

    public static String visible_drop_down_transition_2(String data) {
        return "(//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')])[1]";
    }

    public static String drugAlcoholTestRecordNumber_xpath() {
        return "(//div[@id='form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']//div[contains(text(),'- Record #')])[1]";
    }

    
    public static String Employment_Type() {
        return "//div[@id='control_21267B5E-E90B-4719-A15D-CF40EE711ADB']//ul";
    }

    public static String Person_being_tested() {
        return "//div[@id='control_5D58831B-7752-493F-8EE5-A245C603B428']//ul";
    }

    public static String Person_being_tested_input() {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String SaveBtn() {
        return "//div[@id='btnSave_form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']";
    }

    public static String Linked_incident() {
        return "//div[@id='control_4342AE2C-BC1E-442B-AB70-4E0BF93F2515']";
    }

    public static String Medical_date() {
        return "//div[@id='control_5735F7DF-7695-4096-9ADB-9C21AA9D3FD7']//input";
    }

    public static String Appointment_date() {
        return "//div[@id='control_4E1D0588-3512-41B8-BAF8-DC8517754FEF']//input";
    }

    public static String Person_submit() {
        return "//div[@id='control_711BFF97-3ECC-4FEC-8F3B-F6A3C080684C']//ul";
    }

    public static String Alcohol_result() {
        return "//div[@id='control_C50181BC-31EF-44B6-AD76-D2BE7DFF4844']//ul";
    }

    public static String Drug_result() {
        return "//div[@id='control_E47B8DA0-0088-4009-9B6F-4EE8AA8F96C1']//ul";
    }

    public static String Breath() {
        return "//div[@id='control_2D835DC3-675F-424E-B2B2-1296788AEDD1']//ul";
    }

    public static String Prescribed_Medication() {
        return "//div[@id='control_D8BC29F1-0E65-4DD7-A2D6-554778450AB2']//ul";
    }

    public static String Speech() {
        return "//div[@id='control_7B9E5D4D-8592-41DD-AA6B-86C2FD2EE584']//ul";
    }

    public static String Appearance() {
        return "//div[@id='control_A16196B5-9081-4628-B10B-0F93E869EDFD']//ul";
    }

    public static String employee_stand() {
        return "//div[@id='control_FE431A83-20B7-4E8F-A24A-2A0C2F6F879B']//ul";
    }

    public static String Duty() {
        return "//div[@id='control_7EFD0B08-1252-4EEA-A106-9E629FC38DB2']//ul";
    }

    public static String Eyes() {
        return "//div[@id='control_DD1FF004-B680-4DFD-9859-C62F1D2C6A2B']//ul";
    }

    public static String Behaviour() {
        return "//div[@id='control_651D4389-237B-4372-BD03-FEB33C0807B7']//ul";
    }

    public static String employee_walk() {
        return "//div[@id='control_5D828CA5-6CA8-457C-95A4-B9C294816995']//ul";
    }

    public static String Further_action_required() {
        return "//div[@id='control_0C18B884-2F60-4405-A6DB-6FC63D475D3C']//ul";
    }

    public static String Comments() {
        return "//div[@id='control_672A55A3-439D-4D1B-BB0B-0E09921FDF53']//textarea";
    }

    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String Linked_incident_drop_down() {
        return "//div[@id='control_BE9FE046-7143-4819-8988-0E53681D3AF9']//ul";
    }

}
